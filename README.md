# Beer Calc#

This is a small utility for home-brew beer makers for the approximate calculation of Calories and alcohol content of beer based on the gravity measurements made before and after fermentation. 

### Info ###

* Written in C#