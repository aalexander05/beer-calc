﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BeerCalc
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        public void clear()
        {
            lError.Text = "";
            tbOG.Text = "";
            tbFG.Text = "";
            tbCalories.Text = "";
            tbCarbs.Text = "";
            tbPFG.Text = "";
            tbPOG.Text = "";
            tbAByVolume.Text = "";
            tbAByWeight.Text = "";
            tbRealExtract.Text = "";
            tbApparentAttenuation.Text = "";
            tbRealAttenuation.Text = "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lError.Text = "";
        }

        private void bClear_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void bCalc_Click(object sender, EventArgs e)
        {
            try
            {
                // reset error
                lError.Text = "";

                // degree plato
                double OG = float.Parse(tbOG.Text);
                double FG = float.Parse(tbFG.Text);
                double OGP = (-463.37) + (668.72 * OG) - (205.35 * Math.Pow(OG, 2));
                double FGP = (-463.37) + (668.72 * FG) - (205.35 * Math.Pow(FG, 2));

                tbPOG.Text = Math.Round(OGP, 2).ToString();
                tbPFG.Text = Math.Round(FGP, 2).ToString();

                // real extract
                double RE = (0.1808 * OGP) + (0.8192 * FGP);
                tbRealExtract.Text = Math.Round(RE, 3).ToString();

                // alcohol
                double ABV = (OG - FG) / .75;
                double ABW = (0.79 * ABV) / FG;
                tbAByVolume.Text = Math.Round(ABV * 100, 2).ToString();
                tbAByWeight.Text = Math.Round(ABW * 100, 2).ToString();

                // calories
                double CAL = (6.9 * (ABW * 100) + 4 * (RE - 0.1)) * FG * 3.55;
                tbCalories.Text = Math.Round(CAL, 2).ToString();

                // carbs
                double CARB = ((RE - 0.1) * FG * 3.55);
                tbCarbs.Text = Math.Round(CARB, 2).ToString();

                // attenuation
                double AA = 1 - (FGP / OGP);
                tbApparentAttenuation.Text = Math.Round(AA, 3).ToString();
                double RA = 1 - (RE / OGP);
                tbRealAttenuation.Text = Math.Round(RA, 3).ToString();
            }

            catch
            {
                clear();
                lError.Text = "Error. Try again.";
                tbOG.Focus();
            }

        }
    }
}
