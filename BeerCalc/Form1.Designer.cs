﻿namespace BeerCalc
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bCalc = new System.Windows.Forms.Button();
            this.bClear = new System.Windows.Forms.Button();
            this.lOG = new System.Windows.Forms.Label();
            this.lFG = new System.Windows.Forms.Label();
            this.tbOG = new System.Windows.Forms.TextBox();
            this.tbFG = new System.Windows.Forms.TextBox();
            this.lCalories = new System.Windows.Forms.Label();
            this.lCarbs = new System.Windows.Forms.Label();
            this.tbCalories = new System.Windows.Forms.TextBox();
            this.tbCarbs = new System.Windows.Forms.TextBox();
            this.gbGravity = new System.Windows.Forms.GroupBox();
            this.gbResutls = new System.Windows.Forms.GroupBox();
            this.tbPOG = new System.Windows.Forms.TextBox();
            this.lPlato = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbPFG = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbAByVolume = new System.Windows.Forms.TextBox();
            this.tbAByWeight = new System.Windows.Forms.TextBox();
            this.lAlcohol = new System.Windows.Forms.Label();
            this.lAlcoholByVolume = new System.Windows.Forms.Label();
            this.lAlcoholByWeight = new System.Windows.Forms.Label();
            this.tbRealExtract = new System.Windows.Forms.TextBox();
            this.lRealExtract = new System.Windows.Forms.Label();
            this.tbApparentAttenuation = new System.Windows.Forms.TextBox();
            this.tbRealAttenuation = new System.Windows.Forms.TextBox();
            this.lApparentAttenuation = new System.Windows.Forms.Label();
            this.lRealAttenuation = new System.Windows.Forms.Label();
            this.lError = new System.Windows.Forms.Label();
            this.gbGravity.SuspendLayout();
            this.gbResutls.SuspendLayout();
            this.SuspendLayout();
            // 
            // bCalc
            // 
            this.bCalc.Location = new System.Drawing.Point(253, 23);
            this.bCalc.Name = "bCalc";
            this.bCalc.Size = new System.Drawing.Size(75, 23);
            this.bCalc.TabIndex = 2;
            this.bCalc.Text = "Calculate";
            this.bCalc.UseVisualStyleBackColor = true;
            this.bCalc.Click += new System.EventHandler(this.bCalc_Click);
            // 
            // bClear
            // 
            this.bClear.Location = new System.Drawing.Point(253, 52);
            this.bClear.Name = "bClear";
            this.bClear.Size = new System.Drawing.Size(75, 23);
            this.bClear.TabIndex = 3;
            this.bClear.Text = "Clear";
            this.bClear.UseVisualStyleBackColor = true;
            this.bClear.Click += new System.EventHandler(this.bClear_Click);
            // 
            // lOG
            // 
            this.lOG.AutoSize = true;
            this.lOG.Location = new System.Drawing.Point(7, 16);
            this.lOG.Name = "lOG";
            this.lOG.Size = new System.Drawing.Size(29, 13);
            this.lOG.TabIndex = 1;
            this.lOG.Text = "O.G.";
            // 
            // lFG
            // 
            this.lFG.AutoSize = true;
            this.lFG.Location = new System.Drawing.Point(122, 16);
            this.lFG.Name = "lFG";
            this.lFG.Size = new System.Drawing.Size(27, 13);
            this.lFG.TabIndex = 1;
            this.lFG.Text = "F.G.";
            // 
            // tbOG
            // 
            this.tbOG.Location = new System.Drawing.Point(10, 33);
            this.tbOG.Name = "tbOG";
            this.tbOG.Size = new System.Drawing.Size(67, 20);
            this.tbOG.TabIndex = 0;
            // 
            // tbFG
            // 
            this.tbFG.Location = new System.Drawing.Point(125, 33);
            this.tbFG.Name = "tbFG";
            this.tbFG.Size = new System.Drawing.Size(67, 20);
            this.tbFG.TabIndex = 1;
            // 
            // lCalories
            // 
            this.lCalories.AutoSize = true;
            this.lCalories.Location = new System.Drawing.Point(6, 16);
            this.lCalories.Name = "lCalories";
            this.lCalories.Size = new System.Drawing.Size(44, 13);
            this.lCalories.TabIndex = 1;
            this.lCalories.Text = "Calories";
            // 
            // lCarbs
            // 
            this.lCarbs.AutoSize = true;
            this.lCarbs.Location = new System.Drawing.Point(121, 16);
            this.lCarbs.Name = "lCarbs";
            this.lCarbs.Size = new System.Drawing.Size(34, 13);
            this.lCarbs.TabIndex = 1;
            this.lCarbs.Text = "Carbs";
            // 
            // tbCalories
            // 
            this.tbCalories.Location = new System.Drawing.Point(10, 33);
            this.tbCalories.Name = "tbCalories";
            this.tbCalories.Size = new System.Drawing.Size(67, 20);
            this.tbCalories.TabIndex = 4;
            // 
            // tbCarbs
            // 
            this.tbCarbs.Location = new System.Drawing.Point(124, 33);
            this.tbCarbs.Name = "tbCarbs";
            this.tbCarbs.Size = new System.Drawing.Size(67, 20);
            this.tbCarbs.TabIndex = 5;
            // 
            // gbGravity
            // 
            this.gbGravity.Controls.Add(this.lOG);
            this.gbGravity.Controls.Add(this.lFG);
            this.gbGravity.Controls.Add(this.tbFG);
            this.gbGravity.Controls.Add(this.tbOG);
            this.gbGravity.Location = new System.Drawing.Point(12, 12);
            this.gbGravity.Name = "gbGravity";
            this.gbGravity.Size = new System.Drawing.Size(206, 69);
            this.gbGravity.TabIndex = 3;
            this.gbGravity.TabStop = false;
            this.gbGravity.Text = "Input Gravity Measurements";
            // 
            // gbResutls
            // 
            this.gbResutls.Controls.Add(this.lRealExtract);
            this.gbResutls.Controls.Add(this.lRealAttenuation);
            this.gbResutls.Controls.Add(this.lAlcoholByWeight);
            this.gbResutls.Controls.Add(this.label4);
            this.gbResutls.Controls.Add(this.lApparentAttenuation);
            this.gbResutls.Controls.Add(this.lAlcoholByVolume);
            this.gbResutls.Controls.Add(this.tbRealExtract);
            this.gbResutls.Controls.Add(this.label3);
            this.gbResutls.Controls.Add(this.lAlcohol);
            this.gbResutls.Controls.Add(this.lPlato);
            this.gbResutls.Controls.Add(this.tbRealAttenuation);
            this.gbResutls.Controls.Add(this.lCalories);
            this.gbResutls.Controls.Add(this.tbAByWeight);
            this.gbResutls.Controls.Add(this.tbApparentAttenuation);
            this.gbResutls.Controls.Add(this.tbPFG);
            this.gbResutls.Controls.Add(this.tbAByVolume);
            this.gbResutls.Controls.Add(this.lCarbs);
            this.gbResutls.Controls.Add(this.tbPOG);
            this.gbResutls.Controls.Add(this.tbCarbs);
            this.gbResutls.Controls.Add(this.tbCalories);
            this.gbResutls.Location = new System.Drawing.Point(12, 87);
            this.gbResutls.Name = "gbResutls";
            this.gbResutls.Size = new System.Drawing.Size(333, 208);
            this.gbResutls.TabIndex = 4;
            this.gbResutls.TabStop = false;
            this.gbResutls.Text = "Results";
            // 
            // tbPOG
            // 
            this.tbPOG.Location = new System.Drawing.Point(62, 84);
            this.tbPOG.Name = "tbPOG";
            this.tbPOG.Size = new System.Drawing.Size(67, 20);
            this.tbPOG.TabIndex = 6;
            // 
            // lPlato
            // 
            this.lPlato.AutoSize = true;
            this.lPlato.Location = new System.Drawing.Point(6, 66);
            this.lPlato.Name = "lPlato";
            this.lPlato.Size = new System.Drawing.Size(31, 13);
            this.lPlato.TabIndex = 1;
            this.lPlato.Text = "Plato";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "O.G. =";
            // 
            // tbPFG
            // 
            this.tbPFG.Location = new System.Drawing.Point(62, 109);
            this.tbPFG.Name = "tbPFG";
            this.tbPFG.Size = new System.Drawing.Size(67, 20);
            this.tbPFG.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "F.G. =";
            // 
            // tbAByVolume
            // 
            this.tbAByVolume.Location = new System.Drawing.Point(211, 84);
            this.tbAByVolume.Name = "tbAByVolume";
            this.tbAByVolume.Size = new System.Drawing.Size(67, 20);
            this.tbAByVolume.TabIndex = 8;
            // 
            // tbAByWeight
            // 
            this.tbAByWeight.Location = new System.Drawing.Point(210, 109);
            this.tbAByWeight.Name = "tbAByWeight";
            this.tbAByWeight.Size = new System.Drawing.Size(68, 20);
            this.tbAByWeight.TabIndex = 9;
            // 
            // lAlcohol
            // 
            this.lAlcohol.AutoSize = true;
            this.lAlcohol.Location = new System.Drawing.Point(137, 66);
            this.lAlcohol.Name = "lAlcohol";
            this.lAlcohol.Size = new System.Drawing.Size(53, 13);
            this.lAlcohol.TabIndex = 1;
            this.lAlcohol.Text = "Alcohol %";
            // 
            // lAlcoholByVolume
            // 
            this.lAlcoholByVolume.AutoSize = true;
            this.lAlcoholByVolume.Location = new System.Drawing.Point(149, 87);
            this.lAlcoholByVolume.Name = "lAlcoholByVolume";
            this.lAlcoholByVolume.Size = new System.Drawing.Size(56, 13);
            this.lAlcoholByVolume.TabIndex = 1;
            this.lAlcoholByVolume.Text = "by Volume";
            // 
            // lAlcoholByWeight
            // 
            this.lAlcoholByWeight.AutoSize = true;
            this.lAlcoholByWeight.Location = new System.Drawing.Point(149, 112);
            this.lAlcoholByWeight.Name = "lAlcoholByWeight";
            this.lAlcoholByWeight.Size = new System.Drawing.Size(55, 13);
            this.lAlcoholByWeight.TabIndex = 1;
            this.lAlcoholByWeight.Text = "by Weight";
            // 
            // tbRealExtract
            // 
            this.tbRealExtract.Location = new System.Drawing.Point(10, 168);
            this.tbRealExtract.Name = "tbRealExtract";
            this.tbRealExtract.Size = new System.Drawing.Size(83, 20);
            this.tbRealExtract.TabIndex = 10;
            // 
            // lRealExtract
            // 
            this.lRealExtract.AutoSize = true;
            this.lRealExtract.Location = new System.Drawing.Point(7, 151);
            this.lRealExtract.Name = "lRealExtract";
            this.lRealExtract.Size = new System.Drawing.Size(65, 13);
            this.lRealExtract.TabIndex = 1;
            this.lRealExtract.Text = "Real Extract";
            // 
            // tbApparentAttenuation
            // 
            this.tbApparentAttenuation.Location = new System.Drawing.Point(241, 148);
            this.tbApparentAttenuation.Name = "tbApparentAttenuation";
            this.tbApparentAttenuation.Size = new System.Drawing.Size(69, 20);
            this.tbApparentAttenuation.TabIndex = 11;
            // 
            // tbRealAttenuation
            // 
            this.tbRealAttenuation.Location = new System.Drawing.Point(241, 174);
            this.tbRealAttenuation.Name = "tbRealAttenuation";
            this.tbRealAttenuation.Size = new System.Drawing.Size(69, 20);
            this.tbRealAttenuation.TabIndex = 12;
            // 
            // lApparentAttenuation
            // 
            this.lApparentAttenuation.AutoSize = true;
            this.lApparentAttenuation.Location = new System.Drawing.Point(128, 151);
            this.lApparentAttenuation.Name = "lApparentAttenuation";
            this.lApparentAttenuation.Size = new System.Drawing.Size(107, 13);
            this.lApparentAttenuation.TabIndex = 1;
            this.lApparentAttenuation.Text = "Apparent Attenuation";
            // 
            // lRealAttenuation
            // 
            this.lRealAttenuation.AutoSize = true;
            this.lRealAttenuation.Location = new System.Drawing.Point(149, 177);
            this.lRealAttenuation.Name = "lRealAttenuation";
            this.lRealAttenuation.Size = new System.Drawing.Size(86, 13);
            this.lRealAttenuation.TabIndex = 1;
            this.lRealAttenuation.Text = "Real Attenuation";
            // 
            // lError
            // 
            this.lError.AutoSize = true;
            this.lError.ForeColor = System.Drawing.Color.Red;
            this.lError.Location = new System.Drawing.Point(19, 304);
            this.lError.Name = "lError";
            this.lError.Size = new System.Drawing.Size(106, 13);
            this.lError.TabIndex = 5;
            this.lError.Text = "Error. Clear and retry.";
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 326);
            this.Controls.Add(this.lError);
            this.Controls.Add(this.gbResutls);
            this.Controls.Add(this.gbGravity);
            this.Controls.Add(this.bClear);
            this.Controls.Add(this.bCalc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.Text = "Beer Calc.";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbGravity.ResumeLayout(false);
            this.gbGravity.PerformLayout();
            this.gbResutls.ResumeLayout(false);
            this.gbResutls.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bCalc;
        private System.Windows.Forms.Button bClear;
        private System.Windows.Forms.Label lOG;
        private System.Windows.Forms.Label lFG;
        private System.Windows.Forms.TextBox tbOG;
        private System.Windows.Forms.TextBox tbFG;
        private System.Windows.Forms.Label lCalories;
        private System.Windows.Forms.Label lCarbs;
        private System.Windows.Forms.TextBox tbCalories;
        private System.Windows.Forms.TextBox tbCarbs;
        private System.Windows.Forms.GroupBox gbGravity;
        private System.Windows.Forms.GroupBox gbResutls;
        private System.Windows.Forms.Label lRealExtract;
        private System.Windows.Forms.Label lAlcoholByWeight;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lAlcoholByVolume;
        private System.Windows.Forms.TextBox tbRealExtract;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lAlcohol;
        private System.Windows.Forms.Label lPlato;
        private System.Windows.Forms.TextBox tbAByWeight;
        private System.Windows.Forms.TextBox tbPFG;
        private System.Windows.Forms.TextBox tbAByVolume;
        private System.Windows.Forms.TextBox tbPOG;
        private System.Windows.Forms.Label lRealAttenuation;
        private System.Windows.Forms.Label lApparentAttenuation;
        private System.Windows.Forms.TextBox tbRealAttenuation;
        private System.Windows.Forms.TextBox tbApparentAttenuation;
        private System.Windows.Forms.Label lError;
    }
}

